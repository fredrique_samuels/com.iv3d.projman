package com.iv3d.projman.server;

import com.google.inject.AbstractModule;
import com.iv3d.common.server.inject.RequestPathResolverBuilder;
import com.iv3d.common.server.inject.ResourceHandlerBuilder;
import com.iv3d.common.server.inject.markers.ServerConfigPathMarker;

public class ServerSettingsInjector extends AbstractModule {

	@Override
	protected void configure() {
		bind(RequestPathResolverBuilder.class).to(DefaultRequestPathResolverBuilder.class);
		bind(ResourceHandlerBuilder.class).to(DefaultResourceHandlerBuilder.class);
		bindServerConfig();
	}

	private void bindServerConfig() {
		bind(String.class)
	        .annotatedWith(ServerConfigPathMarker.class)
	        .toInstance("./config/server.settings.json");
	}
}
