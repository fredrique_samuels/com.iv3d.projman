package com.iv3d.projman.server.injection;

import java.io.File;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.iv3d.common.server.inject.settings.DataDirProvider;
import com.iv3d.projman.ProjectFileManagerFactory;
import com.iv3d.projman.projfile.ProjectFileManager;

@Singleton
public class ProjectFileManagerBuilder {
	private ProjectFileManager manager;
	private final DataDirProvider dataRoot;
	private final ProjectFileManagerFactory factory;
	
	@Inject
	public ProjectFileManagerBuilder(DataDirProvider dataRoot,
			ProjectFileManagerFactory factory) {
				this.dataRoot = dataRoot;
				this.factory = factory;
	}
	
	public ProjectFileManager build() {
		if(manager==null) {
			manager = factory.create(new File(dataRoot.get()));
		}
		return manager;
	}
}
