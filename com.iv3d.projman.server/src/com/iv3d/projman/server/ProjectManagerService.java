package com.iv3d.projman.server;

import com.google.inject.Inject;
import com.iv3d.common.server.AbstractService;
import com.iv3d.common.server.inject.ServerProvider;

public final class ProjectManagerService extends AbstractService {

	@Inject
	public ProjectManagerService(ServerProvider provider) {
		super(provider);
	}

}
