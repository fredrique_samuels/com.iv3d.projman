package com.iv3d.projman.server.usecases.projfile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;

import com.google.inject.Inject;
import com.iv3d.common.server.AbstractPathHandler;
import com.iv3d.projman.projfile.ProjectFileManager;
import com.iv3d.projman.projfile.ProjectFileParam;
import com.iv3d.projman.server.injection.ProjectFileManagerBuilder;

public class SaveProjectFileHandler extends AbstractPathHandler {
	private final ProjectFileManager manager;

	@Inject
	public SaveProjectFileHandler(ProjectFileManagerBuilder manager) {
		this.manager = manager.build();
	}

	@Override
	public void handle(Request requestBase, HttpServletRequest request, HttpServletResponse response) {
		ProjectFileParam param = getJsonBody(request, ProjectFileParam.class);
		ProjectFileParam[] pf = manager.get(param.getId());
		if(pf.length==0) {
			String message = "Project with id='%s' does not exists.";
			returnFailure(response, String.format(message, param.getId()));
		} else {
			manager.upsert(param);
			returnOkResponse(response);
		}
	}
	
	@Override
	public String getMethod() {
		return POST;
	}

}
