package com.iv3d.projman.server.usecases.projfile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;

import com.google.inject.Inject;
import com.iv3d.common.server.AbstractPathHandler;
import com.iv3d.projman.projfile.ProjectFileManager;
import com.iv3d.projman.projfile.ProjectFileParam;
import com.iv3d.projman.server.injection.ProjectFileManagerBuilder;

public class ProjectFileIdsHandler extends AbstractPathHandler {

	private final ProjectFileManager manager;

	@Inject
	public ProjectFileIdsHandler(ProjectFileManagerBuilder manager) {
		this.manager = manager.build();
	}

	@Override
	public void handle(Request requestBase, HttpServletRequest request, HttpServletResponse response) {
		String[] idsList = manager.getIdsList();
		ProjectFileParam[] params = manager.get(idsList);
		returnJsonParam(response, params);
	}

	@Override
	public String getMethod() {
		return GET;
	}
}
