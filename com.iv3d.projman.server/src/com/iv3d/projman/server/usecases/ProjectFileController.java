package com.iv3d.projman.server.usecases;

import com.google.inject.Inject;
import com.iv3d.common.server.AbstractController;
import com.iv3d.projman.server.usecases.projfile.CreateProjectFileHandler;
import com.iv3d.projman.server.usecases.projfile.ProjectFileIdsHandler;
import com.iv3d.projman.server.usecases.projfile.ProjectFilesByIdHandler;
import com.iv3d.projman.server.usecases.projfile.SaveProjectFileHandler;

public class ProjectFileController extends AbstractController {

	private CreateProjectFileHandler createProjectFileHandler;
	private SaveProjectFileHandler saveProjectFileHandler;
	private ProjectFileIdsHandler projectFileIdsHandler;
	private ProjectFilesByIdHandler projectFilesByIdHandler;

	@Inject
	public ProjectFileController(CreateProjectFileHandler createProjectFileHandler,
			SaveProjectFileHandler saveProjectFileHandler, ProjectFileIdsHandler projectFileIdsHandler,
			ProjectFilesByIdHandler projectFilesByIdHandler) {
		super();
		this.createProjectFileHandler = createProjectFileHandler;
		this.saveProjectFileHandler = saveProjectFileHandler;
		this.projectFileIdsHandler = projectFileIdsHandler;
		this.projectFilesByIdHandler = projectFilesByIdHandler;
	}

	@Override
	public void configure() {
		set("/com.iv3d.projman/projfile/create", createProjectFileHandler);
		set("/com.iv3d.projman/projfile/save", saveProjectFileHandler);
		set("/com.iv3d.projman/projfile/all", projectFileIdsHandler);
		set("/com.iv3d.projman/projfile/get", projectFilesByIdHandler);
	}

}
