package com.iv3d.projman.server.usecases;

import com.google.inject.Inject;
import com.iv3d.common.server.AbstractController;
import com.iv3d.projman.server.usecases.tag.CreateTagHandler;
import com.iv3d.projman.server.usecases.tag.SaveTagHandler;
import com.iv3d.projman.server.usecases.tag.TagIdsHandler;
import com.iv3d.projman.server.usecases.tag.TagsByIdsHandler;

public class TagController extends AbstractController {

	private final CreateTagHandler createTagHandler;
	private final SaveTagHandler saveTagHandler;
	private final TagIdsHandler tagIdsHandler;
	private final TagsByIdsHandler tagByIdHandler;
	
	@Inject
	public TagController(CreateTagHandler createTagHandler, SaveTagHandler saveTagHandler, TagIdsHandler tagIdsHandler,
			TagsByIdsHandler tagByIdHandler) {
		super();
		this.createTagHandler = createTagHandler;
		this.saveTagHandler = saveTagHandler;
		this.tagIdsHandler = tagIdsHandler;
		this.tagByIdHandler = tagByIdHandler;
	}

	@Override
	public void configure() {
		set("/com.iv3d.projman/tag/create", createTagHandler);
		set("/com.iv3d.projman/tag/save", saveTagHandler);
		set("/com.iv3d.projman/tag/all", tagIdsHandler);
		set("/com.iv3d.projman/tag/get", tagByIdHandler);
	}

}
