package com.iv3d.projman.dao;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import com.iv3d.common.date.UtcClock;

import junit.framework.TestCase;
import mockit.Mocked;
import mockit.NonStrictExpectations;

public class FileSystemDaoTest extends TestCase {

	private static String DATA_ROOT = "iv3d.proj.data";
	private static final File DATA_ROOT_FILE = new File(DATA_ROOT);
	private static String DAO_FOLDER = "test";
	private static File FILE_ROOT = new File(DATA_ROOT, DAO_FOLDER);
	private static File JSON_DATA_FILE = new File(FILE_ROOT, "id.json");
	private static File JSON_DATA_FILE2 = new File(FILE_ROOT, "id2.json");
	private static File JSON_DATA_FILE3 = new File(FILE_ROOT, "id3.json");
	private static File STREAM_DATA_FILE = new File(FILE_ROOT, "id.bin");
	
	
	@Mocked
	private UtcClock clock;
	@Mocked 
	private AuditedDataParam auditedData;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		STREAM_DATA_FILE.delete();
		JSON_DATA_FILE.delete();
		JSON_DATA_FILE2.delete();
		JSON_DATA_FILE3.delete();
		FILE_ROOT.delete();
		DATA_ROOT_FILE.delete();
	}
	
	public void testDataFolderPath() {
		File file = createDao().getDataFolder();
		assertEquals(FILE_ROOT.getPath(), file.getPath());
	}
	
	public void testSave() {
		new NonStrictExpectations() {
			{
				auditedData.getId();
				result = "id";
				
				auditedData.getDateCreated();
				result = "2012-01-01T13:01:01+0000";
			}
		};
		
		assertFalse(JSON_DATA_FILE.exists());
		FileSystemDao<AuditedDataParam> dao = createDao();
		dao.upsert(auditedData);
		assertTrue(JSON_DATA_FILE.exists());
	}
	
	public void testCreationDateCannotChange() {
		
	}
	
	
	public void testGet() {
		new NonStrictExpectations() {
			{
				auditedData.getId();
				result = "id";
				
				auditedData.getDateCreated();
				result = "2012-01-01T13:01:01+0000";
			}
		};

		FileSystemDao<AuditedDataParam> dao = createDao();
		dao.upsert(auditedData);
		
		AuditedDataParam[] actual = dao.get("id");
		
		assertTrue(actual[0] instanceof AuditedDataParam);
		assertEquals("id", actual[0].getId());
		assertEquals("2012-01-01T13:01:01+0000", actual[0].getDateCreated());
	}
	
	public void testSaveInputSteam() {

		new NonStrictExpectations() {
			{
				auditedData.getId();
				result = "id";
				
				auditedData.getDateCreated();
				result = "2012-01-01T13:01:01+0000";
			}
		};
		
		byte[] source = "Data".getBytes();
		ByteArrayInputStream bis = new ByteArrayInputStream(source);
		
		assertFalse(STREAM_DATA_FILE.exists());
		FileSystemDao<AuditedDataParam> dao = createDao();
		dao.upsertStream("id", bis);
		assertTrue(STREAM_DATA_FILE.exists());
	}

	public void testGetStreamById() throws IOException {
		new NonStrictExpectations() {
			{
				auditedData.getId();
				result = "id";
				
				auditedData.getDateCreated();
				result = "2012-01-01T13:01:01+0000";
			}
		};

		FileSystemDao<AuditedDataParam> dao = createDao();

		byte[] source = "Data".getBytes();
		ByteArrayInputStream bis = new ByteArrayInputStream(source);
		dao.upsertStream("id", bis);
		
		InputStream actual = dao.getStreamById("id");
		assertNotNull(actual);
		
		byte[] bytes = new byte[4];
		actual.read(bytes);
		
		assertEquals("Data", new String(bytes));
	}
	
	public void testGetIdsList() {
		new NonStrictExpectations() {
			{	
				auditedData.getDateCreated();
				result = "2012-01-01T13:01:01+0000";
			}
		};

		FileSystemDao<AuditedDataParam> dao = createDao();
		saveRecord(dao, "id");
		saveRecord(dao, "id2");
		saveRecord(dao, "id3");
		
		String[] actual = dao.getIdsList();
		
		assertEquals(3, actual.length);
		assertEquals("id", actual[0]);
		assertEquals("id2", actual[1]);
		assertEquals("id3", actual[2]);
	}
	
	public void testGetMultipleRecords() {
		new NonStrictExpectations() {
			{	
				auditedData.getDateCreated();
				result = "2012-01-01T13:01:01+0000";
			}
		};

		FileSystemDao<AuditedDataParam> dao = createDao();
		saveRecord(dao, "id");
		saveRecord(dao, "id2");
		saveRecord(dao, "id3");
		
		AuditedDataParam[] actual = dao.get("id", "id2", "id6");
		
		assertEquals(2, actual.length);
	}

	private void saveRecord(FileSystemDao<AuditedDataParam> dao, final String id) {
		new NonStrictExpectations() {
			{
				auditedData.getId();
				result = id;
			}
		};
		
		dao.upsert(auditedData);
	}
	
	private FileSystemDao<AuditedDataParam> createDao() {
		return new FileSystemDao<AuditedDataParam>(DATA_ROOT_FILE, DAO_FOLDER, AuditedDataParam.class);
	}
	
}
