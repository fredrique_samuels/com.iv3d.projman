/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.iv3d.projman.tags.domain;

import com.iv3d.common.IdGenerator;
import com.iv3d.common.date.UtcClock;
import com.iv3d.common.date.UtcDate;
import com.iv3d.projman.tags.TagParam;
import com.iv3d.projman.tags.TagFactory;

public final class DefaultTagFactory implements TagFactory {

	private final IdGenerator idGenerator;
	private final UtcClock calander;

	public DefaultTagFactory(UtcClock calander, IdGenerator idGenerator) {
		this.calander = calander;
		this.idGenerator = idGenerator;
	}

	@Override
	public final TagParam create(String value) {
		String id = idGenerator.createId();
		UtcDate createdDate = calander.now();
		TagParam tag = new TagParam();
		tag.setId(id);
		tag.setDateCreated(createdDate.toIso8601String());
		tag.setValue(value);
		return tag;
	}

}
